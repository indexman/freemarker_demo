# 项目介绍

通过用户增删改查的例子来介绍springboot+freemarker的集成使用。

仅供学习交流用。



# 技术栈

- springboot
- mybatis-plus
- freemarker
- bootstrap



# 部署方式

1.解压文件夹，用idea打开

2.创建数据库：datatables_demo， 导入根目录下datatables_demo.sql

3.修改application.yml文件中数据库密码

4.启动项目，访问：localhost:8080/users/list

