package com.test.java.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Description:
 * @Author laoxu
 * @Date 2020/12/17 17:06
 **/
@RequestMapping
@Controller
public class IndexController {
    @RequestMapping("/")
    public String index(){
        return "index";
    }
}
