package com.test.java.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Description:
 * @Author laoxu
 * @Date 2021/1/21 20:57
 **/
@Controller
public class TestController {
    @RequestMapping("/test")
    public String test(ModelMap modelMap){
        modelMap.addAttribute("msg", "Hello laoxu, this is freemarker.");
        return "hello";
    }
}
