package com.test.java.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.test.java.common.Result;
import com.test.java.common.ResultUtil;
import com.test.java.model.User;
import com.test.java.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 用户controller
 * @Author laoxu
 * @Date 2020/12/17 22:40
 **/
@RequestMapping("/user")
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping
    public String user(){
        return "user";
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Result<User> get(@PathVariable Integer id){
        User user =  userService.getById(id);

        return ResultUtil.ok(user);
    }

    /**
     *  分页查询
     * @param username
     * @param pageNo
     * @param pageSize
     * @return
     */
    @PostMapping("/list")
    @ResponseBody
    public Result<IPage<User>> list(@RequestParam(value = "username", required = false) String username,
                                    @RequestParam(defaultValue = "1") Integer pageNo,
                                    @RequestParam(defaultValue = "10") Integer pageSize){
        // 构造查询条件
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(username)){
            queryWrapper.like("username",username);
            queryWrapper.orderByDesc("create_time");
        }
        Page<User> page = new Page<>(pageNo,pageSize);

        IPage<User> result = userService.page(page, queryWrapper);
        // 设置总记录数
        result.setTotal(userService.count(queryWrapper));

        return ResultUtil.ok(result);
    }

    @PostMapping("/add")
    @ResponseBody
    public Result<String> add(@RequestBody User user){
        userService.save(user);

        return ResultUtil.ok("添加成功！");
    }

    @PostMapping("/modify")
    @ResponseBody
    public Result<String> modify(@RequestBody User user){
        userService.saveOrUpdate(user);

        return ResultUtil.ok("修改成功！");
    }

    @PostMapping("/remove")
    @ResponseBody
    public Result<String> remove(@RequestParam Integer id){
        userService.removeById(id);

        return ResultUtil.ok("删除成功！");
    }
}
