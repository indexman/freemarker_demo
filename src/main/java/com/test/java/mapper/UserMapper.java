package com.test.java.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.java.model.User;

/**
 * @Description: 用户Mapper
 * @Author laoxu
 * @Date 2020/12/17 22:32
 **/
public interface UserMapper extends BaseMapper<User> {
}
