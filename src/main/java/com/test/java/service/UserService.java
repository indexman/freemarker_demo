package com.test.java.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.java.model.User;

/**
 * @Description: 用户服务接口
 * @Author laoxu
 * @Date 2020/12/17 22:33
 **/
public interface UserService extends IService<User> {
}
