package com.test.java.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.java.mapper.UserMapper;
import com.test.java.model.User;
import org.springframework.stereotype.Service;

/**
 * @Description: 用户服务实现类
 * @Author laoxu
 * @Date 2020/12/17 22:37
 **/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
