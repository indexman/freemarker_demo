<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>修改用户</title>

    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
</head>
<body>

<div class="container">

    <div class="row">
        <h3 style="text-align: center;">修改用户</h3>
        <form class="form-horizontal" action="/users/save" method="post">
            <div class="form-group">
                <label for="username" class="col-sm-4 control-label">用户名</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="username" name="username" value="${user.username!}">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-4 control-label">邮箱</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="email" name="email" value="${user.email!}">
                </div>
            </div>
            <div class="form-group">
                <label for="truename" class="col-sm-4 control-label">姓名</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="truename" name="truename" value="${user.truename!}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-5 col-sm-8">
                    <button type="button" class="btn btn-default" onclick="cancel()">取消</button>
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
            </div>
        </form>
    </div>

</div>

<script src="/static/js/jquery.min.js"></script>
<script src="/static/js/bootstrap.min.js"></script>
<script src="/static/js/util.js"></script>

<script>

    function cancel() {
        window.location.href="/users/list";
    }
</script>
</body>

</html>