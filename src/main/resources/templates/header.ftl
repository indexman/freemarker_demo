<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>L</b>HY</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>用户</b>管理系统</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">切换导航</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="../static/img/avatar.png" class="user-image" alt="用户图像">
                        <!-- hidden-xs 在小型设备上隐藏用户名，只显示图像。 -->
                        <span class="hidden-xs">欢迎，管理员</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="../static/img/avatar.png" class="img-circle" alt="用户图像">
                            <p>
                                CSDN老徐 - Java全栈工程师
                                <small>注册于2012年11月</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <!--<div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">修改密码</a>
                            </div>-->
                            <!--<div class="pull-right">
                                <a th:href="@{/logout}" class="btn btn-default btn-flat">退出</a>
                            </div>-->
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>