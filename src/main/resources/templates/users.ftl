<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>用户列表</title>

    <link rel="stylesheet" href="../static/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <h3>用户列表</h3>
    <div class="row">
        <div class="form-group">
            <form action="/users/list" method="get">
            <div class="col-sm-4">
                <input type="text" class="form-control" name = "s_username" id="s_username" placeholder="请输入用户名称">
            </div>
            <div class="col-sm-1">
                <button class="btn btn-primary" type="submit" id="search">
                    查询
                </button>
            </div>
            </form>
            <div class="col-sm-1"><button class="btn btn-success" onclick="add()">新增</button></div>

        </div>
    </div>

    <table class="table table-striped" style="margin-top: 10px;">
        <tr>
            <th>序号</th>
            <th>用户名</th>
            <th>邮箱</th>
            <th>姓名</th>
            <th>创建时间</th>
            <th>操作</th>
        </tr>
    <#--遍历用户列表-->
        <#if userList?? && (userList?size >0)>
            <#list userList as user>
            <tr>
                <td>${user_index +1}</td>
                <td>${user.username}</td>
                <td>${user.email}</td>
                <td>${user.truename}</td>
                <td>${(user.createTime?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                <td>
                    <a class="btn btn-sm btn-warning" href="/users/edit/${user.id}">编辑</a>
                    <button class="btn btn-sm btn-danger" onclick="remove(${user.id})">删除</button>
                </td>
            </tr>
            </#list>
        <#else>
        <tr>
            <td colspan="6">无数据</td>
        </tr>
        </#if>
    </table>
</div>

<script src="../static/js/jquery.min.js"></script>
<script src="../static/js/bootstrap.min.js"></script>
<script src="../static/js/util.js"></script>

<script>
    function remove(id) {
        if (confirm("确定删除数据？")) {
            $.ajax({
                type: "POST",
                url: "${ctx.contextPath}remove/"+id,
                success: function (data) {
                    window.location.href="/users/list";
                },
                error: function (e) {
                    console.log("ERROR: ", e);
                }
            });
        }
    }

    function add() {
        window.location.href="/users/edit/0";
    }
</script>
</body>

</html>