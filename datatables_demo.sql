/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : datatables_demo

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2021-01-23 15:09:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `truename` varchar(50) DEFAULT NULL COMMENT '真实姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(100) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('14', 'wuqi', null, 'wuqi@123.com', '武器大师', null, null, '2020-12-24 10:45:28', null);
INSERT INTO `tb_user` VALUES ('15', 'jianmo', null, 'jianmo@123.com', '剑魔', null, null, '2020-12-24 10:45:47', null);
INSERT INTO `tb_user` VALUES ('16', 'angel', null, 'angel@123.com', '天使', null, null, '2020-12-24 10:46:02', null);
INSERT INTO `tb_user` VALUES ('17', 'hzi', null, 'hzi@123.com', '皇子', '2020-12-19 12:22:44', null, '2020-12-24 10:46:23', null);
INSERT INTO `tb_user` VALUES ('18', 'fxy', null, 'fxy@123.com', '王牌飞行员', '2020-12-19 12:27:05', null, '2020-12-24 10:46:42', null);
INSERT INTO `tb_user` VALUES ('20', 'zhangsan', null, '123@123.com', '张三', '2020-12-24 10:34:13', null, '2020-12-24 10:34:13', null);
INSERT INTO `tb_user` VALUES ('21', 'lisi', null, 'lisi@123.com', '李四', '2020-12-24 11:00:19', null, '2020-12-24 11:00:19', null);
INSERT INTO `tb_user` VALUES ('22', 'kasha', null, 'kash@123.com', '卡莎', '2020-12-24 11:00:36', null, '2020-12-24 11:00:36', null);
INSERT INTO `tb_user` VALUES ('27', 'kasha', null, 'kash@123.com', '卡莎', '2021-01-23 14:38:35', null, '2021-01-23 14:38:35', null);
INSERT INTO `tb_user` VALUES ('28', 'kasha1', null, 'kash@123.com', '卡莎1', '2021-01-23 14:41:16', null, '2021-01-23 14:41:16', null);
INSERT INTO `tb_user` VALUES ('30', 'bb', null, 'bb', 'bb', '2021-01-23 14:43:01', null, '2021-01-23 14:43:01', null);
INSERT INTO `tb_user` VALUES ('32', 'bb', null, 'bb', 'bb', '2021-01-23 14:57:19', null, '2021-01-23 14:57:19', null);
